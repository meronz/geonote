package meronz.geonote;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sal on 30/12/15.
 */
public class GeoNote {
    public String title;
    public String text;
    public float latitude;
    public float longitude;

    GeoNote(String mtitle, String mtext, float mlatitude, float mlongitude ){
        this.title = mtitle;
        this.text = mtext;
        this.latitude = mlatitude;
        this.longitude = mlongitude;
    }

}