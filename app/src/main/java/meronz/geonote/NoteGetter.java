package meronz.geonote;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by sal on 31/12/15.
 */
public class NoteGetter {
    private SQLiteDatabase db;
    private boolean isDbOpen = false;
    private Context context;

    NoteGetter(Context mcontext){
        this.context = mcontext;
        db = new DbOpenHelper(context).getWritableDatabase();
    }

    public List<GeoNote> getNoteList() {
        List<GeoNote> list = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT title, descr, lat, lon FROM " + DbOpenHelper.LOCATION_TABLE_NAME ,null);

        if(c.getCount() == 0){
            return list;
        }

        while (c.moveToNext()){
            list.add(new GeoNote(c.getString(0), c.getString(1), 0, 0));
        }

        return list;
    }

    public boolean addNote(GeoNote nNote){
        return true;
    }

    private class DbOpenHelper extends SQLiteOpenHelper {
        private static final String DATABASE_NAME = "Geonote";
        private static final int DATABASE_VERSION = 1;
        private static final String LOCATION_TABLE_NAME = "location";

        private static final String DICTIONARY_TABLE_CREATE =
                "CREATE TABLE IF NOT EXISTS " + LOCATION_TABLE_NAME + " (" +
                        " id INTEGER PRIMARY KEY," +
                        " title TEXT," +
                        " descr TEXT," +
                        " lat REAL," +
                        " lon REAL" +
                        ");";

        DbOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DICTIONARY_TABLE_CREATE);
            // Example values, created only when first starting the application (on db creation)
            // db.execSQL("INSERT INTO location (title, descr) VALUES ('Uno', 'Try1');");
            // db.execSQL("INSERT INTO location (title, descr) VALUES ('Due', 'Try2');");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //TODO: Implement
        }


    }
}
