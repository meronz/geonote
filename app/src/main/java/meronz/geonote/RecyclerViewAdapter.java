package meronz.geonote;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sal on 30/12/15.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.NoteViewHolder>{

    private List<GeoNote> noteList;

    RecyclerViewAdapter(List<GeoNote> mNoteList){
        this.noteList = mNoteList;
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup, false);
        NoteViewHolder noteViewHolder = new NoteViewHolder(v);
        return noteViewHolder;
    }

    @Override
    public void onBindViewHolder(NoteViewHolder noteViewHolder, int i) {
        noteViewHolder.text.setText(noteList.get(i).text);
        noteViewHolder.title.setText(noteList.get(i).title);
    }

    public static class NoteViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView title;
        TextView text;

        NoteViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);
            title = (TextView)itemView.findViewById(R.id.note_title);
            text = (TextView)itemView.findViewById(R.id.note_text);
        }
    }

}